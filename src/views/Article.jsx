// import ARTICLES from "../constants/articles";

const Article = () => {
  const article = {
    /* get article here */
  };
  if (article == null) {
    return (
      <main>
        <h4>Oups</h4>
        <p>Article not found!</p>
      </main>
    );
  }
  return (
    <main>
      <h4>{/* article.title here */}</h4>
      <p>{/* article.description here */}</p>
    </main>
  );
};

export default Article;
