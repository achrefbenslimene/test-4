// import ARTICLES from "../constants/articles";

const Articles = () => {
  return (
    <main>
      <h2>Articles</h2>
      <nav>{/* Articles navlinks here */}</nav>
      {/* Articles outlet here */}
    </main>
  );
};

export default Articles;
