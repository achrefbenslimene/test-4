import React from "react";
import ReactDOM from "react-dom";
import "./index.scss";
import reportWebVitals from "./reportWebVitals";

ReactDOM.render(
  <React.StrictMode>
    <>
      <h1>Test 4</h1>
      {/* Routes here */}
    </>
  </React.StrictMode>,
  document.getElementById("root")
);

reportWebVitals();
